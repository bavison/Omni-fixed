/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef Omni_MakeError_h
#define Omni_MakeError_h

/*
 * MakeError.h
 * Header file for NetFiler client module - part of the Omni project
 */

#include "Filer.h"

typedef enum
{
  err_REGISTERED = ERR_BASE,
  err_BADPROTOCOL,
  err_NOMEM_REGISTRATION,
  err_NOTREGISTERED,
  err_BADREASON,
  err_NEEDNAMES,
  err_NOTPRINTING,
  err_NOMEM_PRINT,
  err_BADPRINT,
  err_BADMOUNTS,
  err_BADCREATE,
  err_NOTIMPLEMENTED,
  err_BADTIME,
  err_NOMEM_BUFFER,
  err_RESTART,

  err_UNKNOWN
} err_number_type;

extern _kernel_oserror *MakeError(int n);
extern _kernel_oserror *OmniError(int n, char *param);
extern void InitErrors(void);
extern void CloseErrors(void);

#endif
